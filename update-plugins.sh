#!/bin/bash

UPGRADE=""

function print_help() {
    echo "usage: ./update-plugins.sh [-h] [--upgrade]"
    echo ""
    echo "Call this script with no parameters to set the installed plugins"
    echo "to the proper commit based on the current repository commit. Or"
    echo "use --upgrade to download the newest (don't forget to push a new"
    echo "commit)."
    echo ""
    echo "    -h, --help    Print this help text."
    echo "    --upgrade     Get the latest from the plugins"
    echo ""
}

while [[ $# -gt 0 ]]; do
    key=$1
    case ${key} in
        -h|--help)
            print_help
            exit 0
            ;;
        --upgrade)
            UPGRADE="--remote"
            ;;
        *)
            (>&2 print_help)
            (>&2 echo "Unknown parameter ${key}")
            exit 1
            ;;
    esac
    shift
done

git submodule update --init --recursive ${UPGRADE}

