set nocompatible			" be iMproved, required
filetype off				" Reset

"=== VUNDLE Settings Start
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
" CWW: Disabling this since this is a submodule
"Plugin 'VundleVim/Vundle.vim'"

"--- Plugins

" A light and configurable statusline/tabline plugin for Vim
Plugin 'itchyny/lightline.vim'

" The NERDTree is a file system explorer for the Vim editor. Using this
" plugin, users can visually browse complex directory hierarchies, quickly
" open files for reading or editing, and perform basic file system operations
Plugin 'https://github.com/scrooloose/nerdtree'

" Add the Sublime option "Multiple Selection" feature into vim. This allows
" for editing multiple parts of the file at the same time such as renaming a
" local variable.
Plugin 'https://github.com/terryma/vim-multiple-cursors'

" Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML
" tags, and more. The plugin provides mappings to easily delete, change and
" add such surroundings in pairs.
"   cs'" - Change the surronding ' with "
"   cst" - Change surrounding tag with " (xml/html tag)
"   <Visual Selection>S" - Surround selection with "
Plugin 'tpope/vim-surround'

" An awesome automatic table creator & formatter allowing one to create neat
" tables as you type.
Plugin 'dhruvasagar/vim-table-mode'

" A Vim plugin which shows a git diff in the 'gutter' (sign column). It shows
" which lines have been added, modified, or removed. You can also preview,
" stage, and undo individual hunks. The plugin also provides a hunk text
" object.
Plugin 'airblade/vim-gitgutter'

" Tool to provide code completion utilizing libclang to index paths. This also
" provides warnings within vim from the compiler.
"
" NOTE: This does require compiling the plugin by calling the install python
" script within the bundle. This may need to be done every once in a while if
" the plugin gets updated.
"     ./install.py --clang-completer
" or
"     ./install.py --clangd-completer
Plugin 'Valloric/YouCompleteMe'

" Plugin to include Confluence Wiki syntax highlighting
Plugin 'confluencewiki.vim'

" Coloring the hash codes for colors so you can see what color it is.
Plugin 'ap/vim-css-color'

" LaTeX plugin suite
Plugin 'lervag/vimtex'

" Syntax highlighting and filetype detection for the 'robot' testing framework.
Plugin 'mfukar/robotframework-vim'

" Asynchronous Linting Engine
Plugin 'w0rp/ale'

" Julia Programming Language
Plugin 'JuliaEditorSupport/julia-vim'

" Graphviz Language
Plugin 'wannesm/wmgraphviz.vim'

call vundle#end()
"=== VUNDLE Settings End

"filetype plugin on			" required
filetype indent plugin on	" required

" === NOTES TO SELF ===
"
" Using "set" will set the setting for the whole instance while using
" "setlocal" will set the setting for the "current buffer" meaning the file
" instance. Use "setlocal" in filetype plugins to avoid cross contamination.
"
" Variables also have their own scopes. Variables are set using "let" and each
" has a prefix that defines the scope. Use the "b:" scope for filetype
" plugins.
"
"    buffer-variable    b:     Local to the current buffer.
"    window-variable    w:     Local to the current window.
"    tabpage-variable   t:     Local to the current tab page.
"    global-variable    g:     Global.
"    local-variable     l:     Local to a function.
"    script-variable    s:     Local to a :source'ed Vim script.
"    function-argument  a:     Function argument (only inside a function).
"    vim-variable       v:     Global, predefined by Vim.
"

set exrc 			" Make VIM Source a .vimrc in the current directory
set secure			" Prevent autocmnd in local vimrc files

" Allow backspacing over everything
set backspace=indent,eol,start
"set nobackup        " Don't keep a backup
set history=50      " Keep 50 lines of history
set ruler           " Show Cursor position
set showcmd         " display incomplete commands
set incsearch       " do incremental searching

if has('mouse')
    set mouse=a " Use the mouse if it has it
endif

" === Color Scheme Settings ===

syntax enable
set t_Co=256                    " Set the color options to 16 or 256
"let g:solarized_termcolors=16   " Use 16 or 256 solarized colors
colorscheme vividchalk          " Use Solarized Colorscheme
hi Comment ctermfg=Cyan         " Because i wasn't satisified with the previous
                                " colors
hi Search ctermfg=gray ctermbg=blue

" === General Editor Settings ===

set hlsearch        " Use highlighting in searches
set tabstop=4       " Tab is 4 Characters
set shiftwidth=4    " number of spaces to use for autoindenting
set number          " Display line numbers
set noexpandtab
"set expandtab       " Make the tabs create spaces instead
set list            " Whitespace characters are made visible
"set lcs=tab:>-,trail:#    " Trailing spaces are shown
set lcs=tab:\·\·,trail:#    " Trailing spaces are shown; don't show tab
set cursorline      " Dislpay highlighting on the row with the cursor
set nowrap          " Do not wrap the text
set colorcolumn=80
set ignorecase      " If disabled, vim will be strict with case.
set smartcase       " Paired with `ignorecase`, will ignore case only if the
                    " pattern uses only lowercase.
"set textwidth=80    " When using `gq` wrap at column 80
set nojoinspaces    " Disables the two spaces after period when using `gq`
set modeline        " Enables the ability to embed settings into a file.
set foldlevel=99    " Level at which folds should be folded when opening files

" === LightLine Settings ===

set laststatus=2
set noshowmode
let g:lightline = {
	\ 'colorscheme' : 'powerline',
	\ 'active' : {
	\   'left' : [ [ 'mode', 'paste' ],
	\              [ 'readonly', 'relativepath', 'modified' ] ],
	\   'right' : [[ 'lineinfo' ],
	\              [ 'percent' ],
	\              [ 'fileformat', 'fileencoding', 'filetype' ]]
	\ },
	\ 'inactive' : {
	\   'left' : [ [ 'relativepath' ] ]
	\ },
    \ }

" === Table Mode ===

" Table Mode Plugin settings for ReST-compatible tables.
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='
" Command for TableMode Emacs Style
command -nargs=0 TMem    let g:table_mode_corner='+' | let g:table_mode_corner_corner='+'
    \ | let g:table_mode_header_fillchar='='
" Command for TableMode Bitbucket Markdown Style
command -nargs=0 TMbb    let g:table_mode_corner='|' | let g:table_mode_corner_corner='|'
    \ | let g:table_mode_header_fillchar='-'

" === YouCompleteMe ===
let g:ycm_filetype_blacklist = {
			\ 'tagbar': 1,
			\ 'notes': 1,
			\ 'markdown': 1,
			\ 'netrw': 1,
			\ 'unite': 1,
			\ 'text': 1,
			\ 'vimwiki': 1,
			\ 'pandoc': 1,
			\ 'infolog': 1,
			\ 'mail': 1,
			\ 'tex': 1
			\}
let g:ycm_filetype_specific_completion_to_disable = {
			\ 'gitcommit': 1,
			\ 'tex': 1
			\}

" === ALE Settings ===

let g:ale_linters = {
			\ 'c': ['clangtidy', 'gcc'],
			\ 'cpp': ['clangtidy', 'g++'],
			\ 'python': ['pylint'],
			\}

let g:ale_c_parse_compile_commands = 1             " Allows parsing cc.json
let g:ale_c_parse_makefile = 0                     " Parse makefile (overrides cc option)
let g:ale_c_build_dir_names = ['build', 'bin']     " Directories where cc.json might be
let g:ale_cpp_parse_compile_commands = 1
let g:ale_cpp_parse_makefile = 0
let g:ale_cpp_build_dir_names = ['build', 'bin']
let g:ale_python_pylint_use_global = 1

" === Custom Commands ===

" Diff Algorithms: myers (default), minimal, patience, histogram
" Command to reset to normal alg
command -nargs=0 DiffReset setlocal diffopt=internal,filler,algorithm:myers
" Command to set patience alg
command -nargs=0 DiffAlt setlocal diffopt=internal,filler,algorithm:patience
command -nargs=0 DiffAlt2 setlocal diffopt=internal,filler,algorithm:histogram

" Clear trailing spaces command
command -nargs=0 Cts %s/\s\+$//e

" Remove Extra Lines - replaces the extra empty line so there is only ever one
" empty line at a time.
command -nargs=0 Rel %s/\n\n\n/\r\r/e

" TODO List using Ctags
" Make sure to periodically regenerate the tags file with `ctags`
command TODO tselect /^todo

" Already defined above with filetype...
set autoindent " AUTO-Indenting!!!

if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                  \ | wincmd p | diffthis
endif

" === Key Mappings

" NerdTree Open/Close menu
nnoremap <F2> :NERDTreeToggle<CR>
vnoremap <F2> <ESC>:NERDTreeToggle<CR>
inoremap <F2> <ESC>:NERDTreeToggle<CR>

" Next error (Used with ALE)
nnoremap ]e :lnext<CR>
nnoremap <C-Right> :lnext<CR>
inoremap <C-Right> <C-o>:lnext<CR>
" Previous error (Used with ALE)
nnoremap [e :lprev<CR>
nnoremap <C-Left> :lprev<CR>
inoremap <C-Left> <C-o>:lprev<CR>

