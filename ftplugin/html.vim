setlocal colorcolumn=80  " Display a column line for manual wordwrap
setlocal nowrap          " Do Not word wrap
setlocal autoindent
setlocal copyindent
setlocal number          " Always show line numbers
setlocal shiftround      " Use multiple shiftwidth when indenting with '</>'
setlocal hlsearch        " Highlight search terms

" TAB Settings
setlocal expandtab       " Use spaces instead of tab
setlocal tabstop=2       " Tab is 2 characters (standard for kernel work is 8)
setlocal shiftwidth=2    " Number of spaces to use for autoindenting
setlocal softtabstop=2   " Backspaces will delete a tab worth of spaces

" Folding
setlocal foldmethod=syntax " How to determine folds

