README
======

Global Plugins
--------------

These plugins load up the settings for every use of vim. The plugins are
installed when they are placed within the plugin directory.

    ~/.vim/plugin

### Vundle

Vundle is a vim plugin manager, which itself is a plugin, that is able to pull
updates and install plugins from GitHub directly using git. In this case the
repositories that it pulls down I am adding as submodules.

The plugins are added by using the `Plugin` command in the `.vimrc` followed by
the relative path on GitHub (i.e. what comes after <https://github.com/>).

After the line is added in the file, restart vim and run `:PluginInstall`. This
will pull down the plugin repository in to the `~/vim/bundle` directory and
install it. The repository can then be added to this repository as a submodule.

File Type Plugins
-----------------

These type of plugins load up the settings inside only when the file extension
matches that of the plugin name. These plugins are installed if they are in the
file type plugin directory.

    ~/.vim/ftplugin


Plugins Installed
-----------------

- [Table Mode][tablemode] : An awesome automatic table creator & formatter
  allowing one to create neat tables as you type.
- [Light Line][lightline] : Formats the status bar to be easier to read and
  understand.
- [Nerd Tree][nerdtree] : Provides a directory structure to be able to navigate
  from within vim.
- [Multiple Cursors][multicursor] : Provides a feature from Sublime that allows
  editing with multiple cursors.
- [surround.vim][] : Utility to manage surrounding a selection with characters
  such as quote marks and whatnot.
- [gitgutter][] : A Vim plugin which shows a git diff in the 'gutter' (sign
  column). It shows which lines have been added, modified, or removed.
- [YouCompleteMe][] : An auto completion plugin to aid in C/C++ development by
  utilizing `libclang` to index the project.

[tablemode]: https://github.com/dhruvasagar/vim-table-mode
[lightline]: https://github.com/itchyny/lightline.vim
[nerdtree]: https://github.com/scrooloose/nerdtree
[multicursor]: https://github.com/terryma/vim-multiple-cursors
[surround.vim]: https://github.com/tpope/vim-surround
[gitgutter]: https://github.com/airblade/vim-gitgutter
[YouCompleteMe]: https://github.com/Valloric/YouCompleteMe

